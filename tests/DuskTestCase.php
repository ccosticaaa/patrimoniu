<?php

namespace Tests;


use App\Http\Controllers\Admin\Item\ItemService;
use App\User;
use Laravel\Dusk\TestCase as BaseTestCase;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use ReflectionProperty;

abstract class DuskTestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Prepare for Dusk test execution.
     *
     * @beforeClass
     * @return void
     */
    public static function prepare()
    {
        static::startChromeDriver();
    }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return \Facebook\WebDriver\Remote\RemoteWebDriver
     */
    protected function driver()
    {
        $options = (new ChromeOptions)->addArguments([
            '--disable-gpu',
            '--headless',
            '--window-size=1600,800',
        ]);

        return RemoteWebDriver::create(
            'http://localhost:9515', DesiredCapabilities::chrome()->setCapability(
                ChromeOptions::CAPABILITY, $options
            )
        );
    }

    public function getSuperAdmin(){
        return User::where('email', 'admin@example.com')->first()->id;
    }

//    public function getIdByName($name){
////         return ItemService::all()->where('name', $name)->first()->id;
//
//        $service = app()->make(ItemService::class);
//
//        $itemModelRP = new ReflectionProperty(ItemService::class, 'itemModel');
//        $itemModelRP->setAccessible(true);
//        $itemModel = $itemModelRP->getValue($service);
//        return $itemModel->where('name',$name)->first()->id;
//
//    }
}
