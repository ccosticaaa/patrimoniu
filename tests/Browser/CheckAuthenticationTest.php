<?php

namespace Tests\Browser;

use App\User;
use Illuminate\Support\Facades\App;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CheckAuthenticationTest extends DuskTestCase
{
    /**
     * @group check-auth
     */
    public function test()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit( '/items')
                    ->assertPathIs('/login')
                ;
        });
    }

    /**
     * @group check-auth
     */
    public function testLoginAndAllow()
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->visit( '/items')
                ->assertPathIs( '/login')
                ->loginAs($this->getSuperAdmin())
                ->visit( '/items')
                ->assertPathIs( '/items')
                ->logout()
            ;
        });
    }

    /**
     * @group check-auth
     */
    public function testLoginAndLogout()
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->visit( '/items')
                ->assertPathIs( '/login')
                ->loginAs($this->getSuperAdmin())
                ->visit( '/items')
                ->assertPathIs( '/items')
                ->logout()
                ->visit( '/items')
                ->assertPathIs( '/login')
            ;
        });
    }

}
