<?php


namespace Tests\Browser;

use App\Http\Controllers\Admin\Category\CategoryService;
use App\Http\Controllers\Admin\Item\ItemService;
use App\User;
use Laravel\Dusk\Browser;
use phpDocumentor\Reflection\DocBlock\Tags\Property;
use ReflectionProperty;
use Tests\DuskTestCase;

class CreateCategoryTest extends DuskTestCase
{

    /*
    tests if the Create Category page works
    */
    public function testCategoryPage()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit( '/categories')
                    ->assertPathIs( '/login')
                    ->loginAs(User::find(1))
                    ->visit( '/categories')
                    ->assertPathIs( '/categories')
                    ->logout()
            ;
        });
    }

    /*
    tests if the Create Category button redirects to Create Category page
    */
    public function testAddCategoryButton()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit( '/categories')
                    ->assertPathIs( '/login')
                    ->loginAs(User::find(1))
                    ->visit( '/categories')
                    ->click('#redirect-to-create-category')
                    ->assertPathIs( '/categories/create')
                    ->logout()

            ;
        });
    }


    /*
    tests if the Category was created successfully
    */
    public function testCreateCategory(){
        $this->browse(function (Browser $browser) {
            $name = "test-category".time();
            $browser->visit( '/categories')
                ->assertPathIs( '/login')
                ->loginAs(User::find(1))
                ->visit( '/categories')
                ->click('#redirect-to-create-category')
                ->assertPathIs( '/categories/create')
                ->type('name', $name )
                ->click('button[type*="submit"]')
                ->waitForRoute('categories.edit', [$this->getIdByName($name)], 5)
                ->assertPathIs( '/categories/'.$this->getIdByName($name).'/edit')
                ->assertSee("Category successfully created")
                ->logout()
            ;
        });
    }


    /*
     tests the Edit Action
     */
    public function testEditAction(){
        $this->browse(function (Browser $browser) {
            $name = "test-category".time();
            $name_edited = "test-category-edited".time();
            $browser->visit( '/categories')
                ->assertPathIs( '/login')
                ->loginAs(User::find(1))
                ->visit( '/categories')
                ->click('#redirect-to-create-category')
                ->assertPathIs( '/categories/create')
                ->type('name', $name )
                ->click('button[type*="submit"]')
                ->waitForRoute('categories.edit', [$this->getIdByName($name)], 5)
                ->visit( '/categories')
                ->assertPathIs( '/categories')
                ->click('a[href*="/categories/'.$this->getIdByName($name).'/edit"]')
                ->assertPathIs( '/categories/'.$this->getIdByName($name).'/edit')
                ->type('name', $name_edited )
                ->click('button[type*="submit"]')
                ->pause(2000)
                ->assertSee("Category successfully updated")
                ->logout()
            ;
        });
    }


    /*
    tests the Delete Action
    */
    public function testDeleteAction(){
        $this->browse(function (Browser $browser) {
            $name = "test-category".time();
            $browser->visit( '/categories')
                ->assertPathIs( '/login')
                ->loginAs(User::find(1))
                ->visit( '/categories')
                ->click('#redirect-to-create-category')
                ->assertPathIs( '/categories/create')
                ->type('name', $name )
                ->click('button[type*="submit"]')
                ->visit( '/categories')
                ->assertPathIs( '/categories')
                ->click('a[href*="/categories/'.$this->getIdByName($name).'/delete"]')
                ->assertPathIs( '/categories')
                ->assertDontSee('$name')
                ->pause(2000)
                ->assertPathIs( '/categories')
                ->logout()
            ;
        });
    }


    /*
    tests the warning message when no name is written at Creation of Category
    */
    public function testWarningNameAtCreateCategory(){
        $this->browse(function (Browser $browser) {
//            $name = "test-category".time();
            $browser->visit( '/categories')
                ->assertPathIs( '/login')
                ->loginAs(User::find(1))
                ->visit( '/categories')
                ->click('#redirect-to-create-category')
                ->assertPathIs( '/categories/create')
                ->type('name', '' )
                ->click('button[type*="submit"]')
                ->pause(2000)
                ->assertSee("#1 A name is required.")
                ->logout()
            ;
        });
    }

    /*
    tests the warning message when no name is written at Creation of Category
    */


    /*
    finds a Category by its name
    */
    public function getIdByName($name){

        $service = app()->make(CategoryService::class);
        $categoryModelRP = new ReflectionProperty(CategoryService::class, 'categoryModel');
        $categoryModelRP->setAccessible(true);
        $categoryModel = $categoryModelRP->getValue($service);
        return $categoryModel->where('name', $name)->first()->id;
    }
}
