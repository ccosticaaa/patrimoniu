<?php
/**
 * Created by PhpStorm.
 * User: Zorro Andrei
 * Date: 25-Mar-20
 * Time: 22:06
 */

namespace Tests\Browser\Item;


use App\Http\Controllers\Admin\Category\CategoryService;
use App\Http\Controllers\Admin\Item\ItemService;
use ReflectionProperty;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

class EditItemTest extends DuskTestCase
{
    public function testGoToPageButton()
    {
        $this->browse(function (Browser $browser) {
            $name1 = 'Test-Edit-1-' . time();
            $browser
                ->visit( '/login')
                ->assertPathIs( '/login')
                ->loginAs($this->getSuperAdmin())
                ->visit( '/items')
                ->assertPathIs( '/items')
                ->click('#redirect-to-create-item')
                ->assertPathIs( '/items/create')
                ->type('name',$name1)
                ->type('page_title',$name1)
                ->type('#create-item-form > div > div.card-body > div:nth-child(6) > div > div > div > div.fr-wrapper.show-placeholder > div', $name1)
                ->click('#add_item')
                ->waitForRoute('items.edit', [$this->getIdByName($name1)], 5)

                ->visit('/items')
                ->click('a[href*="'.$this->getIdByName($name1).'/edit"]')
                ->assertRouteIs('items.edit', [$this->getIdByName($name1)])
                ->logout()
            ;
        });
    }

    public function testEditName()
    {
        $this->browse(function (Browser $browser) {
            $name1 = 'Test-Edit-1-' . time();
            $name2 = 'Test-Edit-2-' . time();
            $browser
                ->visit( '/login')
                ->assertPathIs( '/login')
                ->loginAs($this->getSuperAdmin())
                ->visit( '/items')
                ->assertPathIs( '/items')
                ->click('#redirect-to-create-item')
                ->assertPathIs( '/items/create')
                ->type('name',$name1)
                ->type('page_title',$name1)
                ->type('#create-item-form > div > div.card-body > div:nth-child(6) > div > div > div > div.fr-wrapper.show-placeholder > div', $name1)
                ->click('#add_item')
                ->waitForRoute('items.edit', [$this->getIdByName($name1)], 5)

                ->visit('/items')
                ->click('a[href*="'.$this->getIdByName($name1).'/edit"]')
                ->assertRouteIs('items.edit', [$this->getIdByName($name1)])
                ->type('name',$name2)
                ->click('#save_item')
                ->assertSee('Item Updated Successfully!')
                ->visit( '/items')
                ->assertPathIs( '/items')
                ->assertSee($name2)
                ->logout()
            ;
        });
    }


    public function testEditCategory()
    {
        $this->browse(function (Browser $browser) {
            $name1 = 'Test-Edit-1-' . time();
            $browser
                ->visit( '/login')
                ->assertPathIs( '/login')
                ->loginAs($this->getSuperAdmin())
                ->visit( '/items')
                ->assertPathIs( '/items')
                ->click('#redirect-to-create-item')
                ->assertPathIs( '/items/create')
                ->type('name',$name1)
                ->type('page_title',$name1)
                ->type('#create-item-form > div > div.card-body > div:nth-child(6) > div > div > div > div.fr-wrapper.show-placeholder > div', $name1)
                ->click('#add_item')
                ->waitForRoute('items.edit', [$this->getIdByName($name1)], 5)

                ->visit('/items')
                ->click('a[href*="'.$this->getIdByName($name1).'/edit"]')
                ->assertRouteIs('items.edit', [$this->getIdByName($name1)])
                ->select('category_id',''.$this->getOtherCategoryId().'')
                ->click('#save_item')
                ->assertSee('Item Updated Successfully!')
                ->assertNotSelected('category_id',''.$this->getDefaultCategoryId().'')
                ->logout()
            ;
        });
    }

    public function testEditTitle()
    {
        $this->browse(function (Browser $browser) {
            $name1 = 'Test-Edit-1-' . time();
            $name2 = 'Test-Edit-2-' . time();
            $browser
                ->visit( '/login')
                ->assertPathIs( '/login')
                ->loginAs($this->getSuperAdmin())
                ->visit( '/items')
                ->assertPathIs( '/items')
                ->click('#redirect-to-create-item')
                ->assertPathIs( '/items/create')
                ->type('name',$name1)
                ->type('page_title',$name1)
                ->type('#create-item-form > div > div.card-body > div:nth-child(6) > div > div > div > div.fr-wrapper.show-placeholder > div', $name1)
                ->click('#add_item')
                ->waitForRoute('items.edit', [$this->getIdByName($name1)], 5)

                ->visit('/items')
                ->click('a[href*="'.$this->getIdByName($name1).'/edit"]')
                ->assertRouteIs('items.edit', [$this->getIdByName($name1)])
                ->type('page_title',$name2)
                ->click('#save_item')
                ->assertSee('Item Updated Successfully!')
                ->logout()
            ;
        });
    }


    public function testEditContent()
    {
        $this->browse(function (Browser $browser) {
            $name1 = 'Test-Edit-1-' . time();
            $name2 = 'Test-Edit-2-' . time();
            $browser
                ->visit( '/login')
                ->assertPathIs( '/login')
                ->loginAs($this->getSuperAdmin())
                ->visit( '/items')
                ->assertPathIs( '/items')
                ->click('#redirect-to-create-item')
                ->assertPathIs( '/items/create')
                ->type('name',$name1)
                ->type('page_title',$name1)
                ->type('#create-item-form > div > div.card-body > div:nth-child(6) > div > div > div > div.fr-wrapper.show-placeholder > div', $name1)
                ->click('#add_item')
                ->waitForRoute('items.edit', [$this->getIdByName($name1)], 5)

                ->visit('/items')
                ->click('a[href*="'.$this->getIdByName($name1).'/edit"]')
                ->assertRouteIs('items.edit', [$this->getIdByName($name1)])
                ->type('#edit-item-form > div > div.card-body > div:nth-child(9) > div > div > div > div.fr-wrapper > div', $name2)
                ->click('#save_item')
                ->assertSee('Item Updated Successfully!')
                ->logout()
            ;
        });
    }

    public function model(){
        $service = app()->make(ItemService::class);

        $itemModelRP = new ReflectionProperty(ItemService::class, 'itemModel');
        $itemModelRP->setAccessible(true);
        return $itemModelRP->getValue($service);
    }

    public function modelCategory(){
        $service = app()->make(CategoryService::class);

        $categoryModelRP = new ReflectionProperty(CategoryService::class, 'categoryModel');
        $categoryModelRP->setAccessible(true);
        return $categoryModelRP->getValue($service);
    }

    public function getIdByName($name){
        $itemModel = $this->model();
        return $itemModel->where('name',$name)->first()->id;
    }

    public function  getDefaultCategoryId()
    {
        $categoryModel = $this->modelCategory();
        $category = $categoryModel->where('name', 'DEFAULT CATEGORY')->first();
        return $category->id;
    }

    public function  getOtherCategoryId(){
        $categoryModel = $this->modelCategory();
        $category = $categoryModel->where('name',"!=",'DEFAULT CATEGORY')->first();
        return $category->id;
    }
}
