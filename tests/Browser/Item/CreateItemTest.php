<?php
/**
 * Created by PhpStorm.
 * User: Zorro Andrei
 * Date: 22-Mar-20
 * Time: 23:14
 */

namespace Tests\Browser\Item;

use App\Http\Controllers\Admin\Category\CategoryService;
use App\Http\Controllers\Admin\Item\ItemService;
use ReflectionProperty;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

class CreateItemTest extends DuskTestCase
{
    /**
     * @group create-item
     */
    public function testGoToPageButton()
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->visit( '/login')
                ->assertPathIs( '/login')
                ->loginAs($this->getSuperAdmin())
                ->visit( '/items')
                ->assertPathIs( '/items')
                ->click('#redirect-to-create-item')
                ->assertPathIs( '/items/create')
                ->logout()
            ;
        });
    }

    public function testAddItemWithoutRequiredFields()
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->visit( '/login')
                ->assertPathIs( '/login')
                ->loginAs($this->getSuperAdmin())
                ->visit( '/items')
                ->assertPathIs( '/items')
                ->click('#redirect-to-create-item')
                ->assertPathIs( '/items/create')
                ->click('#add_item')
                ->assertSee('A name is required.')
                ->assertSee('The page title must be a string')
                ->logout()
            ;
        });
    }

    public function testAddDefaultItem()
    {
        $this->browse(function (Browser $browser) {
            $name = 'Test-Create-' . time();
            $browser
                ->visit( '/login')
                ->assertPathIs( '/login')
                ->loginAs($this->getSuperAdmin())
                ->visit( '/items')
                ->assertPathIs( '/items')
                ->click('#redirect-to-create-item')
                ->assertPathIs( '/items/create')
                ->type('name',$name)
                ->type('page_title',$name)
                ->type('#create-item-form > div > div.card-body > div:nth-child(6) > div > div > div > div.fr-wrapper.show-placeholder > div', $name)
                ->click('#add_item')
                ->assertSee('Item Created Successfully!')
                ->waitForRoute('items.edit', [$this->getIdByName($name)], 5)
                ->assertRouteIs('items.edit', [$this->getIdIfHasDefaultImage($name)])
                ->assertSelected('category_id',''.$this->getDefaultCategoryId().'')
                ->logout()
            ;
        });
    }

    public function testAddItemRandomCategory()
    {
        $this->browse(function (Browser $browser) {
            $name = 'Test-Create-' . time();
            $browser
                ->visit( '/login')
                ->assertPathIs( '/login')
                ->loginAs($this->getSuperAdmin())
                ->visit( '/items')
                ->assertPathIs( '/items')
                ->click('#redirect-to-create-item')
                ->assertPathIs( '/items/create')
                ->type('name',$name)
                ->select('category_id',''.$this->getOtherCategoryId().'')
                ->type('page_title',$name)
                ->type('#create-item-form > div > div.card-body > div:nth-child(6) > div > div > div > div.fr-wrapper.show-placeholder > div', $name)
                ->click('#add_item')
                ->assertSee('Item Created Successfully!')
                ->waitForRoute('items.edit', [$this->getIdByName($name)], 5)
                ->assertRouteIs('items.edit', [$this->getIdIfHasDefaultImage($name)])
                ->assertNotSelected('category_id',''.$this->getDefaultCategoryId().'')
                ->logout()
            ;
        });
    }

    public function model(){
        $service = app()->make(ItemService::class);

        $itemModelRP = new ReflectionProperty(ItemService::class, 'itemModel');
        $itemModelRP->setAccessible(true);
        return $itemModelRP->getValue($service);
    }
    public function modelCategory(){
        $service = app()->make(CategoryService::class);

        $categoryModelRP = new ReflectionProperty(CategoryService::class, 'categoryModel');
        $categoryModelRP->setAccessible(true);
        return $categoryModelRP->getValue($service);
    }


    public function getIdByName($name){
        $itemModel = $this->model();
        return $itemModel->where('name',$name)->first()->id;
    }

    public function getIdIfHasDefaultImage($name){
        $itemModel = $this->model();
        $id = $this->getIdByName($name);
        $item = $itemModel->where('id',$id)->where('image_path','like','%placeholder2%')->first();
        return $item->id;
    }

    public function  getDefaultCategoryId(){
        $categoryModel = $this->modelCategory();
        $category = $categoryModel->where('name','DEFAULT CATEGORY')->first();
        return $category->id;
    }

    public function  getOtherCategoryId(){
        $categoryModel = $this->modelCategory();
        $category = $categoryModel->where('name',"!=",'DEFAULT CATEGORY')->first();
        return $category->id;
    }
}
