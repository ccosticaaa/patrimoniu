<?php
/**
 * Created by PhpStorm.
 * User: Zorro Andrei
 * Date: 26-Mar-20
 * Time: 0:31
 */

namespace Tests\Browser\Item;


use App\Http\Controllers\Admin\Item\ItemService;
use Laravel\Dusk\Browser;
use ReflectionProperty;
use Tests\DuskTestCase;

class DeleteItemTest extends DuskTestCase
{


    public function testDeleteButton()
    {
        $this->browse(function (Browser $browser) {
            $name = 'Test-Delete-' . time();
            $browser
                ->visit( '/login')
                ->assertPathIs( '/login')
                ->loginAs($this->getSuperAdmin())
                ->visit( '/items')
                ->assertPathIs( '/items')
                ->click('#redirect-to-create-item')
                ->assertPathIs( '/items/create')
                ->type('name',$name)
                ->type('page_title',$name)
                ->type('#create-item-form > div > div.card-body > div:nth-child(6) > div > div > div > div.fr-wrapper.show-placeholder > div', $name)
                ->click('#add_item')
                ->waitForRoute('items.edit', [$this->getIdByName($name)], 5)

                ->visit('/items')
                ->click('a[href*="'.$this->getIdByName($name).'/delete"]')
                ->assertSee('Item successfully deleted')
                ->assertPathIs( '/items')
                ->assertDontSee($name)
                ->logout()
            ;
        });
    }

    public function model(){
        $service = app()->make(ItemService::class);

        $itemModelRP = new ReflectionProperty(ItemService::class, 'itemModel');
        $itemModelRP->setAccessible(true);
        return $itemModelRP->getValue($service);
    }

    public function getIdByName($name){
        $itemModel = $this->model();
        return $itemModel->where('name',$name)->first()->id;
    }


}
