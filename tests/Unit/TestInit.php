<?php

namespace Tests\Unit;

use App\Http\Controllers\Admin\Category\Category;
use App\Http\Controllers\Admin\Category\CategoryService;

use App\Http\Controllers\Admin\Item\Item;
use App\Http\Controllers\Admin\Item\ItemService;

use Illuminate\Foundation\Testing\DatabaseTransactions;

use Illuminate\Http\Request;
use Illuminate\Http\Testing\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

use ReflectionException;
use ReflectionProperty;

use Tests\TestCase;

class TestInit extends TestCase
{
    use DatabaseTransactions;

    /**
     * @var string
     */
    protected $categoryTable;

    /**
     * @var string
     */
    protected $itemTable;

    /**
     * @var CategoryService|null
     */
    protected $categoryService;

    /**
     * @var ItemService|null
     */
    protected $itemService;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        try
        {
            $this->setCategoryTable();
        }
        catch (ReflectionException $e)
        {
            $this->assertTrue(false);
        }

        try
        {
            $this->setItemTable();
        }
        catch (ReflectionException $e)
        {
            $this->assertTrue(false);
        }

        $this->setCategoryService();
        $this->setItemService();
    }

    /**
     * Get and set the database table to be used along with these tests.
     *
     * @return void
     * @throws ReflectionException
     */
    public function setCategoryTable()
    {
        $service = app()->make(CategoryService::class);

        $categoryModelRP = new ReflectionProperty(CategoryService::class, 'categoryModel');
        $categoryModelRP->setAccessible(true);
        $categoryModel = $categoryModelRP->getValue($service);

        $this->categoryTable = $categoryModel->getTable();

        $status = ($this->categoryTable === 'categories');
        $this->assertTrue($status);
    }

    /**
     * Get and set the database table to be used along with these tests.
     *
     * @return void
     * @throws ReflectionException
     */
    public function setItemTable()
    {
        $service = app()->make(ItemService::class);

        $itemModelRP = new ReflectionProperty(ItemService::class, 'itemModel');
        $itemModelRP->setAccessible(true);
        $itemModel = $itemModelRP->getValue($service);

        $this->itemTable = $itemModel->getTable();

        $status = ($this->itemTable === 'items');
        $this->assertTrue($status);
    }

    /**
     * Get and set the database table to be used along with these tests.
     *
     * @return void
     */
    public function setCategoryService()
    {
        $service = app()->make(CategoryService::class);

        $this->categoryService = $service;

        $status = ($service instanceof CategoryService);
        $this->assertTrue($status);
    }

    /**
     * Get and set the database table to be used along with these tests.
     *
     * @return void
     */
    public function setItemService()
    {
        $service = app()->make(ItemService::class);

        $this->itemService = $service;

        $status = ($service instanceof ItemService);
        $this->assertTrue($status);
    }

    /**
     * Create category function to be used in future category tests
     *
     * @return Category
     */
    public function createCategory() : Category
    {
        $categoryServiceInstance = $this->categoryService;

        $requestObject = [
            'name' => Str::random(rand(1, 190)),
        ];

        $requestBag = new Request();
        $requestBag->setMethod('POST');
        $requestBag->request->add($requestObject);

        return $categoryServiceInstance->store($requestBag);
    }

    /**
     * Create item function to be used in future category tests
     *
     * @param $params array
     *
     * @return Item
     */
    public function createItem(array $params = []) : Item
    {
        $itemServiceInstance = $this->itemService;

        $requestObject = array_merge($this->itemDefaultParams(), $params);

        $requestBag = new Request();
        $requestBag->setMethod('POST');
        $requestBag->request->add($requestObject);

        return $itemServiceInstance->store($requestBag);
    }

    /**
     * Default required params to create an item
     *
     * @return array
     */
    public function itemDefaultParams() : array
    {
        return [
            'name' => Str::random(rand(1, 190)),
            'category_id' => DEFAULT_CATEGORY_ID,
            'page_title' => Str::random(rand(1, 190)),
            'page_body' => '<p>' . Str::random(rand(1, 190)) . '</p>',
        ];
    }

    /**
     * Mock an image to be used when creating an item
     *
     * @param $name string
     * @param $width int
     * @param $height int
     *
     * @return File
     */
    public function mockFormImage(string $name = 'mockImage1.jpg', int $width = 10, int $height = 10) : File
    {
        return UploadedFile::fake()->image($name, $width, $height);
    }
}
