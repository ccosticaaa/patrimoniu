<?php


namespace Tests\Unit;


use Illuminate\Support\Facades\Storage;

class DeleteItemTest extends TestInit
{
    /**
     * Soft delete an existing item
     *
     * @returns void
     */
    public function testSoftDeleteItem()
    {
        $item = parent::createItem();
        $this->assertDatabaseHas($this->itemTable, $item->toArray());
        $this->assertDatabaseHas($this->itemTable, [
            'id' => $item->id,
            'category_id' => DEFAULT_CATEGORY_ID,
        ]);

        $softDeleteStatus = $this->itemService->destroy($item);
        $this->assertTrue($softDeleteStatus);
        $this->assertSoftDeleted($this->itemTable, $item->toArray());
        $this->assertSoftDeleted($this->itemTable, [
            'id' => $item->id,
            'category_id' => DEFAULT_CATEGORY_ID,
        ]);

        /*
         * on item soft delete, we don't delete the generated files,
         * so we can still check for file existence
         */
        Storage::disk('uploads')->assertExists($item->image_path);
        Storage::disk('uploads')->assertExists($item->qr_code_path);
        Storage::disk('uploads')->assertExists($item->print_file_path);
    }
}
