<?php


namespace Tests\Unit;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UpdateItemTest extends TestInit
{
    /**
     *  Update the name of an existing item
     *
     * @return void
     */
    public function testUpdateItemName()
    {
        $item = parent::createItem();
        $this->assertDatabaseHas($this->itemTable, $item->toArray());
        $this->assertDatabaseHas($this->itemTable, [
            'id' => $item->id,
            'category_id' => DEFAULT_CATEGORY_ID,
        ]);

        Storage::disk('uploads')->assertExists($item->image_path);
        Storage::disk('uploads')->assertExists($item->qr_code_path);
        Storage::disk('uploads')->assertExists($item->print_file_path);

        $requestObject = [
            'name' => Str::random(rand(1, 190)),
            'page_title' => $item->page_title,
            'page_body' => $item->page_body,
        ];

        $requestBag = new Request();
        $requestBag->setMethod('POST');
        $requestBag->request->add($requestObject);

        $updateStatus = $this->itemService->update($requestBag, $item);
        $this->assertTrue($updateStatus);
        $this->assertDatabaseHas($this->itemTable, $item->toArray());
        $this->assertDatabaseHas($this->itemTable, [
            'id' => $item->id,
            'name' => $item->name,
        ]);

        Storage::disk('uploads')->assertExists($item->image_path);
        Storage::disk('uploads')->assertExists($item->qr_code_path);
        Storage::disk('uploads')->assertExists($item->print_file_path);
    }

    /**
     *  Update the category of an existing item
     *
     * @return void
     */
    public function testUpdateItemCategory()
    {
        $category = parent::createCategory();
        $this->assertDatabaseHas($this->categoryTable, $category->toArray());

        $item = parent::createItem();
        $this->assertDatabaseHas($this->itemTable, $item->toArray());
        $this->assertDatabaseHas($this->itemTable, [
            'id' => $item->id,
            'category_id' => DEFAULT_CATEGORY_ID,
        ]);

        Storage::disk('uploads')->assertExists($item->image_path);
        Storage::disk('uploads')->assertExists($item->qr_code_path);
        Storage::disk('uploads')->assertExists($item->print_file_path);

        $requestObject = [
            'category_id' => $category->id,
            'page_title' => $item->page_title,
            'page_body' => $item->page_body,
        ];

        $requestBag = new Request();
        $requestBag->setMethod('POST');
        $requestBag->request->add($requestObject);

        $updateStatus = $this->itemService->update($requestBag, $item);
        $this->assertTrue($updateStatus);
        $this->assertDatabaseHas($this->itemTable, $item->toArray());
        $this->assertDatabaseHas($this->itemTable, [
            'id' => $item->id,
            'category_id' => $category->id,
        ]);

        Storage::disk('uploads')->assertExists($item->image_path);
        Storage::disk('uploads')->assertExists($item->qr_code_path);
        Storage::disk('uploads')->assertExists($item->print_file_path);
    }

    /**
     *  Update the image of an existing item
     *
     * @return void
     */
    public function testUpdateItemImage()
    {
        $item = parent::createItem();
        $this->assertDatabaseHas($this->itemTable, $item->toArray());
        $this->assertDatabaseHas($this->itemTable, [
            'id' => $item->id,
            'category_id' => DEFAULT_CATEGORY_ID,
        ]);

        Storage::disk('uploads')->assertExists($item->image_path);
        Storage::disk('uploads')->assertExists($item->qr_code_path);
        Storage::disk('uploads')->assertExists($item->print_file_path);

        $requestObject = [
            'image' => $this->mockFormImage('testUpdateItemImage.jpg',20, 20),
            'page_title' => $item->page_title,
            'page_body' => $item->page_body,
        ];

        $requestBag = new Request();
        $requestBag->setMethod('POST');
        $requestBag->request->add($requestObject);

        $updateStatus = $this->itemService->update($requestBag, $item);
        $this->assertTrue($updateStatus);
        $this->assertDatabaseHas($this->itemTable, $item->toArray());

        Storage::disk('uploads')->assertExists($item->image_path);
        Storage::disk('uploads')->assertExists($item->qr_code_path);
        Storage::disk('uploads')->assertExists($item->print_file_path);
    }

    /**
     *  Update the page title of an existing item
     *
     * @return void
     */
    public function testUpdateItemPageTitle()
    {
        $item = parent::createItem();
        $this->assertDatabaseHas($this->itemTable, $item->toArray());
        $this->assertDatabaseHas($this->itemTable, [
            'id' => $item->id,
            'category_id' => DEFAULT_CATEGORY_ID,
        ]);

        Storage::disk('uploads')->assertExists($item->image_path);
        Storage::disk('uploads')->assertExists($item->qr_code_path);
        Storage::disk('uploads')->assertExists($item->print_file_path);

        $requestObject = [
            'page_title' => Str::random(rand(1, 190)),
            'page_body' => $item->page_body,
        ];

        $requestBag = new Request();
        $requestBag->setMethod('POST');
        $requestBag->request->add($requestObject);

        $updateStatus = $this->itemService->update($requestBag, $item);
        $this->assertTrue($updateStatus);
        $this->assertDatabaseHas($this->itemTable, $item->toArray());
        $this->assertDatabaseHas($this->itemTable, [
            'id' => $item->id,
            'page_title' => $item->page_title,
        ]);

        Storage::disk('uploads')->assertExists($item->image_path);
        Storage::disk('uploads')->assertExists($item->qr_code_path);
        Storage::disk('uploads')->assertExists($item->print_file_path);
    }

    /**
     *  Update the page body of an existing item
     *
     * @return void
     */
    public function testUpdateItemPageBody()
    {
        $item = parent::createItem();
        $this->assertDatabaseHas($this->itemTable, $item->toArray());
        $this->assertDatabaseHas($this->itemTable, [
            'id' => $item->id,
            'category_id' => DEFAULT_CATEGORY_ID,
        ]);

        Storage::disk('uploads')->assertExists($item->image_path);
        Storage::disk('uploads')->assertExists($item->qr_code_path);
        Storage::disk('uploads')->assertExists($item->print_file_path);

        $requestObject = [
            'page_title' => $item->page_title,
            'page_body' => '<p>' . Str::random(rand(1, 190)) . '</p>',
        ];

        $requestBag = new Request();
        $requestBag->setMethod('POST');
        $requestBag->request->add($requestObject);

        $updateStatus = $this->itemService->update($requestBag, $item);
        $this->assertTrue($updateStatus);
        $this->assertDatabaseHas($this->itemTable, $item->toArray());
        $this->assertDatabaseHas($this->itemTable, [
            'id' => $item->id,
            'page_body' => $item->page_body,
        ]);

        Storage::disk('uploads')->assertExists($item->image_path);
        Storage::disk('uploads')->assertExists($item->qr_code_path);
        Storage::disk('uploads')->assertExists($item->print_file_path);
    }

    /**
     *  Perform a full update (update all possible fields) of an existing item
     *
     * @return void
     */
    public function testFullUpdateItem()
    {
        $category = parent::createCategory();
        $this->assertDatabaseHas($this->categoryTable, $category->toArray());

        $item = parent::createItem();
        $this->assertDatabaseHas($this->itemTable, $item->toArray());
        $this->assertDatabaseHas($this->itemTable, [
            'id' => $item->id,
            'category_id' => DEFAULT_CATEGORY_ID,
        ]);

        Storage::disk('uploads')->assertExists($item->image_path);
        Storage::disk('uploads')->assertExists($item->qr_code_path);
        Storage::disk('uploads')->assertExists($item->print_file_path);

        $requestObject = [
            'name' => Str::random(rand(1, 190)),
            'category_id' => $category->id,
            'image' => $this->mockFormImage('testItemFullUpdateImage.jpg',30, 30),
            'page_title' => Str::random(rand(1, 190)),
            'page_body' => '<p>' . Str::random(rand(1, 190)) . '</p>',
        ];

        $requestBag = new Request();
        $requestBag->setMethod('POST');
        $requestBag->request->add($requestObject);

        $updateStatus = $this->itemService->update($requestBag, $item);
        $this->assertTrue($updateStatus);
        $this->assertDatabaseHas($this->itemTable, $item->toArray());
        $this->assertDatabaseHas($this->itemTable, [
            'id' => $item->id,
            'name' => $item->name,
            'category_id' => $category->id,
            'page_title' => $item->page_title,
            'page_body' => $item->page_body,
        ]);

        Storage::disk('uploads')->assertExists($item->image_path);
        Storage::disk('uploads')->assertExists($item->qr_code_path);
        Storage::disk('uploads')->assertExists($item->print_file_path);
    }
}
