<?php

namespace Tests\Unit;


use Illuminate\Support\Facades\Storage;

class CreateItemTest extends TestInit
{
    /**
     * Create an item, assign it to default category and store it in the database.
     *
     * @return void
     */
    public function testCreateItemDefaultCategory()
    {
        $item = parent::createItem();
        $this->assertDatabaseHas($this->itemTable, $item->toArray());
        $this->assertDatabaseHas($this->itemTable, [
            'id' => $item->id,
            'category_id' => DEFAULT_CATEGORY_ID,
        ]);

        Storage::disk('uploads')->assertExists($item->image_path);
        Storage::disk('uploads')->assertExists($item->qr_code_path);
        Storage::disk('uploads')->assertExists($item->print_file_path);
    }

    /**
     * Create an item, assign it to an existing category and store it in the database.
     *
     * @return void
     */
    public function testCreateItemExistingCategory()
    {
        $category = parent::createCategory();
        $this->assertDatabaseHas($this->categoryTable, $category->toArray());

        $item = parent::createItem([
            'category_id' => $category->id,
        ]);
        $this->assertDatabaseHas($this->itemTable, $item->toArray());
        $this->assertDatabaseHas($this->itemTable, [
            'id' => $item->id,
            'category_id' => $category->id,
        ]);

        Storage::disk('uploads')->assertExists($item->image_path);
        Storage::disk('uploads')->assertExists($item->qr_code_path);
        Storage::disk('uploads')->assertExists($item->print_file_path);
    }

    /**
     * Create an item, assign it to an existing category and store it in the database.
     *
     * @return void
     */
    public function testCreateItemWithImage()
    {
        $item = parent::createItem([
            'image' => $this->mockFormImage(),
        ]);
        $this->assertDatabaseHas($this->itemTable, $item->toArray());

        Storage::disk('uploads')->assertExists($item->image_path);
        Storage::disk('uploads')->assertExists($item->qr_code_path);
        Storage::disk('uploads')->assertExists($item->print_file_path);
    }
}
