<?php


namespace Tests\Unit;


use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UpdateCategoryTest extends TestInit
{
    /**
     * Update an existing category and store the changes in the database
     *
     * @return void
     */
    public function testUpdateCategory()
    {
        $category = parent::createCategory();

        $this->assertDatabaseHas($this->categoryTable, $category->toArray());

        $requestObject = [
            'name' => Str::random(rand(1, 190)),
        ];

        $requestBag = new Request();
        $requestBag->setMethod('POST');
        $requestBag->request->add($requestObject);

        $this->categoryService->update($requestBag, $category);

        $this->assertDatabaseHas($this->categoryTable, $category->toArray());
    }
}
