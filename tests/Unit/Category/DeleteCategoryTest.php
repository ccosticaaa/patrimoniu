<?php


namespace Tests\Unit;


class DeleteCategoryTest extends TestInit
{
    /**
     * Soft delete an existing category
     *
     * @return void
     */
    public function testSoftDeleteCategory()
    {
        $category = parent::createCategory();
        $this->assertDatabaseHas($this->categoryTable, $category->toArray());

        $this->categoryService->destroy($category);
        $this->assertSoftDeleted($this->categoryTable, $category->toArray());
    }

    /**
     * Soft delete an existing category and unbind items linked to the deleted category
     *
     * @return void
     */
    public function testSoftDeleteCategoryUnbindItems()
    {
        $category = parent::createCategory();
        $this->assertDatabaseHas($this->categoryTable, $category->toArray());

        $item = parent::createItem([
            'category_id' => $category->id,
        ]);
        $this->assertDatabaseHas($this->itemTable, $item->toArray());
        $this->assertDatabaseHas($this->itemTable, [
            'category_id' => $category->id,
        ]);

        $this->categoryService->destroy($category);
        $this->assertSoftDeleted($this->categoryTable, $category->toArray());

        $this->itemService->unbindCategoryItems($category);
        $this->assertDatabaseMissing($this->itemTable, [
            'category_id' => $category->id,
        ]);
        $this->assertDatabaseHas($this->itemTable, [
            'category_id' => DEFAULT_CATEGORY_ID,
        ]);
    }
}
