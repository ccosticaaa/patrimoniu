<?php

namespace Tests\Unit;

use App\Http\Controllers\Admin\Category\Category;
use App\Http\Controllers\Admin\Category\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateCategoryTest extends TestInit
{

    /**
     * Create a category and store it in the database.
     *
     * @return void
     */
    public function testCreateCategory()
    {
        $category = parent::createCategory();

        $this->assertDatabaseHas($this->categoryTable, $category->toArray());
    }
}
