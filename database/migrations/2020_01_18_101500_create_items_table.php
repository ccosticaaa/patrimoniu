<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');
            $table->string('name');
            $table->string('uuid'); // populate at item creation, not modifiable
            $table->string('page_title'); // what we display when some1 scans the qr code
            $table->longText('page_body'); // item information, could be html ? todo :: check if wysiwyg editor works ok
            $table->string('image_path'); // modifiable, we upload the image with an unique name and set the path in the database
            $table->string('qr_code_path'); // modifiable, we upload the image with an unique name and set the path in the database
            $table->string('print_file_path'); // let's see if it is easier to generate the file each time or at item creation.
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
