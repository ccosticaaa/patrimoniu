<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\User::firstOrCreate([
            'email' => 'admin@example.com',
            'password' => \Illuminate\Support\Facades\Hash::make('asd123'),
            'name' => 'SuperAdmin',
        ]);

        \App\Http\Controllers\Admin\Category\Category::firstOrCreate([
            'name' => 'DEFAULT CATEGORY',
        ]);
    }
}
