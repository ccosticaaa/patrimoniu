<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Admin\Item\Item;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getItemInformation($itemUUID){

        $item = Item::where('uuid', $itemUUID)->first();

        return json_encode($item);
    }
}
