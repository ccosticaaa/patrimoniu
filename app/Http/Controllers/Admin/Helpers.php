<?php
/**
 * Created by PhpStorm.
 * User: Gabriel-Marian Hanci
 * Date: 24-Jan-20
 * Time: 21:19
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\Item\Item;
use Illuminate\Support\Str;

class Helpers
{
    public static function getUploadedImageFilename($request)
    {
        $filename = $request['uuid'];
        $format = $request['image']->getClientOriginalExtension();

        return $filename . '.' . $format;
    }

    public static function generateImageStoragePath($request)
    {
        $filename = self::getUploadedImageFilename($request);

        return ITEM_IMAGE_PATH . $filename;
    }

    public static function generatePDFStoragePath($filename, $format)
    {
        return ITEM_PDF_FULL_PATH . $filename . '.' . $format;
    }

    public static function generatePDFPath($filename, $format)
    {
        return ITEM_PDF_PATH . $filename . '.' . $format;
    }

    public static function generateQrCodeStoragePath($filename, $format)
    {
        return QRCODE_IMAGES_FULL_PATH . $filename . '.' . $format;
    }

    public static function generateQrCodePath($filename, $format)
    {
        return QRCODE_IMAGES_PATH . $filename . '.' . $format;
    }

    public static function generateUUID($ordered = false)
    {
        $uuid = null;

        switch ($ordered)
        {
            case true:
                $uuid = Str::orderedUuid()->toString();
                break;
            case false:
                $uuid = Str::uuid()->toString();
                break;
            default:
                $uuid = Str::uuid()->toString();
                break;
        }

        return $uuid;
    }
}