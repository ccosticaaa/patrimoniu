<?php
/**
 * Created by PhpStorm.
 * User: Gabriel-Marian Hanci
 * Date: 19-Jan-20
 * Time: 0:54
 */

namespace App\Http\Controllers\Admin\Category;

use App\Http\Controllers\Admin\Item\ItemService;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{

    protected $categoryService;
    protected $itemService;

    public function __construct(CategoryService $categoryService, ItemService $itemService)
    {
        $this->categoryService = $categoryService;
        $this->itemService = $itemService;
    }

    public function index()
    {
        $categories = $this->categoryService->all();
        return view('categories.index')->with('categories', $categories);
    }

    public function show(Admin $admin)
    {
        return view('show')->with('admin', $admin);
    }

        public function create()
    {

        return view('categories.create');
    }

    public function store(CategoryValidator $request)
    {
        $category = $this->categoryService->store($request);

        return redirect()->route('categories.edit',[$category])->with('status', !false)->with('message','Category successfully created');
    }

    public function edit(Category $category)
    {
        $response = $this->categoryService->edit($category);

        return view('categories.edit')->with("category",$response);
    }

    public function update(CategoryValidator $request, Category $category)
    {
       $status = $this->categoryService->update($request, $category);

        return redirect()->route('categories.edit',[$category])->with('status', $status)->with('message','Category successfully updated');
    }

    public function destroy(Category $category)
    {
        $statusItems = $this->itemService->unbindCategoryItems($category);
        $statusCategory = $this->categoryService->destroy($category);
        $status = ($statusItems && $statusCategory);

        return redirect()->route('categories.index')->with('status', $status)->with('message','Category successfully deleted');
    }
}
