<?php
/**
 * Created by PhpStorm.
 * User: Gabriel-Marian Hanci
 * Date: 21-Jan-20
 * Time: 0:13
 */

namespace App\Http\Controllers\Admin\Category;


class CategoryService
{
    protected $categoryModel;

    public function __construct(Category $category)
    {
        $this->categoryModel = $category;
    }

    public function all()
    {
        $response = $this->categoryModel->all();
        return $response;
    }

    public function store($request)
    {
        $request = $request->all();

        return $this->categoryModel->create($request);
    }

    public function edit($category)
    {
        $dbCategory = $this->categoryModel->where( 'id' ,$category->id)->first();

        return $dbCategory;
    }

    public function update($request, Category $category)
    {
        return $category->update($request->all());
    }

    public function destroy(Category $category)
    {
        return $category->delete();
    }
}
