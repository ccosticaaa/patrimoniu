<?php
/**
 * Created by PhpStorm.
 * User: Zorro Andrei
 * Date: 26-Jan-20
 * Time: 17:51
 */

namespace App\Http\Controllers\Admin\Category;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CategoryValidator extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'name' => ['required', 'string', 'max:191', 'unique:categories,name'],
                    ];
                }
            case 'PUT':
                {}
            case 'PATCH':
                {
                    $category = request()->category;
                    return [
                        'name' => ['required', 'string', 'max:191', Rule::unique('categories', 'name')->ignore($category->id)],
                    ];
                }
            default:
                break;
        }
    }

    public function messages()
    {
        return [
            'name.required'     => 'A name is required.',
            'name.max'          => 'The name cannot have more than 191 characters',
            'name.unique'       => 'The name is already in use!',
        ];
    }

}