<?php
/**
 * Created by PhpStorm.
 * User: Gabriel-Marian Hanci
 * Date: 21-Jan-20
 * Time: 0:05
 */

namespace App\Http\Controllers\Admin\Category;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $table = 'categories';

    public $timestamps = true;

    protected $fillable = [
        'name',
    ];

    public function items()
    {
        return $this->hasMany('App\Http\Controllers\Admin\Item\Item');
    }
}