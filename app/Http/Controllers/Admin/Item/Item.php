<?php
/**
 * Created by PhpStorm.
 * User: Gabriel-Marian Hanci
 * Date: 21-Jan-20
 * Time: 0:03
 */

namespace App\Http\Controllers\Admin\Item;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class Item extends Model
{
    use SoftDeletes;

    protected $table = 'items';

    public $timestamps = true;

    protected $fillable = [
        'category_id',
        'name',
        'uuid',
        'page_title',
        'page_body',
        'image_path',
        'qr_code_path',
        'print_file_path',
    ];

    public function category()
    {
        return $this->belongsTo('App\Http\Controllers\Admin\Category\Category');
    }

    public function getPageBodyAttribute($value)
    {
        return htmlspecialchars_decode($value);
    }


}
