<?php
/**
 * Created by PhpStorm.
 * User: Gabriel-Marian Hanci
 * Date: 21-Jan-20
 * Time: 0:12
 */

namespace App\Http\Controllers\Admin\Item;


use App\Http\Controllers\Admin\Helpers;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use QrCode, PDF;

class ItemService
{
    protected $itemModel;

    private $clientUrl = 'https://costica.dev/';

    public function __construct(Item $item)
    {
        $this->itemModel = $item;
    }

    public function all()
    {
        $response = $this->itemModel->all();
        return $response;
    }

    public function edit(Item $item)
    {
        return $this->itemModel->where('id', $item->id)->first();
    }

    /*
     * name, category_id, and page title can be straight updated, they're just plain text
     * image update must be handled individually before regenerating pdf (if needed)
     * if anything our pdf contains (page_title, image, page_body) changes, we must regenerate pdf
     * */
    public function update($request, Item $item)
    {
        $request = $request->all();

        /*
         * before updating, check if pdf must be regenerated
         * */
        $regeneratePDF = $this->PDFRegenerationRequired($request, $item);

        if (isset($request['image']))
        {
            $request['uuid'] = $item->uuid;
            $request['image_path'] = $this->storeImage($request);
        }

        $request = $this->removeUnnecessaryFields($request);

        /*
         * update whatever we have so far so we have latest data for the pdf regeneration
         * */
        $status = $item->update($request);
        $item->touch();

        /*
         * regenerate pdf based on latest data
         * we do not need to update the path cuz it's the same stuff
         * */
        if ($regeneratePDF)
        {
            $this->generateAndStoreItemPDF($item);
            $this->generateAndStoreQrCodePDF($item);
        }

        return $status;
    }

    public function destroy(Item $item)
    {
        return $item->delete();
    }

    public function PDFRegenerationRequired($request, $item)
    {
        $response = false;

        if ($request['page_title'] !== $item->page_title)
        {
            $response = true;
        }
        if ($request['page_body'] !== $item->page_body)
        {
            $response= true;
        }
        if (isset($request['image']))
        {
            $response = true;
        }

        return $response;
    }

    public function store($request)
    {
        $request = $request->all();

        $request = $this->generateFields($request);

        $request = $this->removeUnnecessaryFields($request);

        $item = $this->itemModel->create($request);
        return $item;
    }

    public function removeUnnecessaryFields($request)
    {
        $modelColumns = Schema::getColumnListing('items');

        foreach ($request as $column => $value)
        {
            if (!in_array($column, $modelColumns))
            {
                unset($request[$column]);
            }
        }

        return $request;
    }

    public function convertHTMLFields($request)
    {
        $request['page_body'] = htmlspecialchars($request['page_body']);
        return $request;
    }

    public function generateFields($request)
    {
        $uuid = $this->generateValidUUID(true);
        $request['uuid'] = $uuid;

        $qrCodePaths = $this->generateAndStoreQrCode($request, 'png');
        $request['qr_code_path'] = $qrCodePaths['path'];

        $imagePath = $this->storeImage($request);
        $request['image_path'] = $imagePath;

        $pdfPaths = $this->generateAndStoreItemPDF($request);
        $request['print_file_path'] = $pdfPaths['path'];

        $this->generateAndStoreQrCodePDF($request);

        return $request;
    }

    public function generateAndStoreQrCodePDF($item)
    {
        $response = [];

        $filename = $item['uuid'] . '_only_qr';
        $fullPath = Helpers::generatePDFStoragePath($filename, 'pdf');

        $response['fullPath'] = $fullPath;
        $response['path'] = Helpers::generatePDFPath($filename, 'pdf');

        /*
         * delete file if it already exists (this is for update item cases)
         * this is needed because the pdf composer package can't overwrite existing files
         * */
        $this->deleteFileIfExists($response['path']);

        $this->generateQrCodePDF($item)->save($fullPath);

        return $response;
    }

    public function generateAndStoreItemPDF($item)
    {
        $response = [];

        $filename = $item['uuid'];
        $fullPath = Helpers::generatePDFStoragePath($filename, 'pdf');

        $response['fullPath'] = $fullPath;
        $response['path'] = Helpers::generatePDFPath($filename, 'pdf');

        /*
         * delete file if it already exists (this is for update item cases)
         * this is needed because the pdf composer package can't overwrite existing files
         * */
        $this->deleteFileIfExists($response['path']);

        $this->generateItemPDF($item)->save($fullPath);

        return $response;
    }

    public function deleteFileIfExists($path)
    {
        $exists = Storage::disk('uploads')->exists($path);
        if ($exists)
        {
            Storage::disk('uploads')->delete($path);
        }
    }

    public function generateQrCodePDF($item)
    {
        $pdf = PDF::loadView('items.qrcode_pdf', [
            'item' => $item,
        ]);

        return $pdf;
    }

    public function generateItemPDF($item)
    {
        $pdf = PDF::loadView('items.qrcode_with_info_pdf', [
            'item' => $item,
        ]);

        return $pdf;
    }

    /*
     * returns [
     *  'fullPath' => full path to the file on the server
     *  'path' => relative path to public
     * ]the full path to the file on the server
     * */

    private function getQrUrl($uuid){
        return $this->clientUrl . $uuid;
    }

    public function generateAndStoreQrCode($item, $format = 'png')
    {
        $response = [];

        $uuid = $item['uuid'];

        $fullPath = Helpers::generateQrCodeStoragePath($uuid, $format);

        $response['fullPath'] = $fullPath;

        QrCode::errorCorrection('H')
            ->format($format)
            ->size(QR_CODE_SIZE)
            ->margin(QR_CODE_MARGINS)
            ->generate($this->getQrUrl($uuid), $fullPath);

        $response['path'] = Helpers::generateQrCodePath($uuid, $format);

        return $response;
    }

    public function generateValidUUID($ordered = false)
    {
        $uuid = null;

        $uuid = Helpers::generateUUID($ordered);

        while (Item::where('uuid', $uuid)->count() !== 0)
        {
            $uuid = Helpers::generateUUID($ordered);
        }

        return $uuid;
    }

    public function storeImage($request)
    {
        $path = null;

        //if an image has been uploaded in form
        if (isset($request['image']))
        {
            $storagePath = Helpers::generateImageStoragePath($request);
            $this->deleteFileIfExists($storagePath);
            $filename = Helpers::getUploadedImageFilename($request);
            $path = '/' . Storage::disk('uploads')->putFileAs(__DB__ITEM_IMAGE_PATH, $request['image'], $filename);
        }
        //else provide path to placeholder
        else
        {
            $path = PLACEHOLDER_DEFAULT_IMAGE_PATH;
        }

        return $path;
    }

    public function unbindCategoryItems($category)
    {
        $status = $this->itemModel->where('category_id', $category->id)
            ->update([
                'category_id' => DEFAULT_CATEGORY_ID,
            ]);

        return $status;
    }
}
