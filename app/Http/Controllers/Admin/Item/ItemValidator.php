<?php
/**
 * Created by PhpStorm.
 * User: Gabriel-Marian Hanci
 * Date: 24-Jan-20
 * Time: 19:25
 */

namespace App\Http\Controllers\Admin\Item;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ItemValidator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method())
        {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'name' => ['required', 'string', 'max:191', 'unique:items,name'],
                        'image' => ['image', 'mimes:jpeg,png,jpg', 'max:2048'],
                        'page_title' => ['string', 'max:191'],
                    ];
                }
            case 'PUT':
                {}
            case 'PATCH':
                {
                    $item = request()->item;
                    return [
                        'name' => ['required', 'string', 'max:191', Rule::unique('items', 'name')->ignore($item->id)],
                        'image' => ['image', 'mimes:jpeg,png,jpg', 'max:2048'],
                        'page_title' => ['string', 'max:191'],
                    ];
                }
            default:
                break;
        }
    }

    public function messages()
    {
        return [
            'name.required'     => 'A name is required.',
            'name.max'          => 'The name cannot have more than 191 characters',
            'name.unique'       => 'The name is already in use!',
            'image.mimes'       => 'The image must respect any of this formats:jpeg,png,jpg',
            'image.image'       => 'The uploaded file must be an image.',
            'image.max'         => 'Image size is too large.',
            'page_title.max'    => 'The title cannot have more than 191 characters',
        ];
    }


}