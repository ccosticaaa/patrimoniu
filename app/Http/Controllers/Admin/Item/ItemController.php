<?php
/**
 * Created by PhpStorm.
 * User: Gabriel-Marian Hanci
 * Date: 18-Jan-20
 * Time: 23:42
 */

namespace App\Http\Controllers\Admin\Item;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\Item\ItemService;
use App\Http\Controllers\Admin\Category\CategoryService;
use Illuminate\Support\Facades\Cache;

class ItemController extends Controller
{

    protected $categoryService;
    protected $itemService;

    public function __construct(ItemService $itemService, CategoryService $categoryService)
    {
        $this->itemService = $itemService;
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        $response = $this->itemService->all();
        return view('items.index')->with('items', $response);
    }

    public function edit(Item $item)
    {
        $response = $this->itemService->edit($item);
        $categories = $this->categoryService->all();

        return view('items.edit')
            ->with('item', $response)
            ->with('categories', $categories);
    }

    public function show(Item $item)
    {
        return view('items.show')->with('item', $item);
    }

    public function store(ItemValidator $request)
    {
        $item = $this->itemService->store($request);
        $status = true;
        $message = 'Item Created Successfully!';

        return redirect()->route('items.edit', [$item])
            ->with('status', $status)
            ->with('message', $message);
    }

    public function update(ItemValidator $request, Item $item)
    {
        $status = $this->itemService->update($request, $item);
        $message = 'Item Updated Successfully!';

        return redirect()->route('items.edit', [$item])
            ->with('status', $status)
            ->with('message', $message);
    }

    public function destroy(Item $item)
    {
        $status = $this->itemService->destroy($item);

        return redirect()->route('items.index')->with('status', $status)->with('message',"Item successfully deleted");
    }

    public function create()
    {
        $categories = $this->categoryService->all();
        return view('items.create')->with('categories', $categories);
    }
}
