<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- Material Dashboard CSS -->
    <link rel="stylesheet" href="{{asset('md4p/css/material-dashboard.min.css?v=2.1.1')}}">

    @yield('style')

</head>
<body>
    <div class="wrapper">
        @include('sidebar')
        <main class="c-main">
            <div class="main-panel ps-container ps-theme-default" data-ps-id="cb706d96-7f28-5236-d2fc-5ef09c121b52">
                @include('topnavbar')
                <div class="content">
                    @yield('content')
                </div>
            </div>
            <!-- Main content here -->
        </main>
    </div>

    <!--   Core JS Files   -->
    <script src="{{asset('md4p/js/core/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('md4p/js/core/popper.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('md4p/js/core/bootstrap-material-design.min.js')}}" type="text/javascript"></script>

    <!-- Plugin for the Perfect Scrollbar -->
    <script src="{{asset('md4p/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>

    <!-- Plugin for the momentJs  -->
    <script src="{{asset('md4p/js/plugins/moment.min.js')}}"></script>

    <!--  Plugin for Sweet Alert -->
    <script src="{{asset('md4p/js/plugins/sweetalert2.js')}}"></script>

    <!-- Forms Validations Plugin -->
    <script src="{{asset('md4p/js/plugins/jquery.validate.min.js')}}"></script>

    <!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
    <script src="{{asset('md4p/js/plugins/jquery.bootstrap-wizard.js')}}"></script>

    <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
    <script src="{{asset('md4p/js/plugins/bootstrap-selectpicker.js')}}" ></script>

    <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
    <script src="{{asset('md4p/js/plugins/bootstrap-datetimepicker.min.js')}}"></script>

    <!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->
    <script src="{{asset('md4p/js/plugins/jquery.datatables.min.js')}}"></script>

    <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
    <script src="{{asset('md4p/js/plugins/bootstrap-tagsinput.js')}}"></script>

    <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
    <script src="{{asset('md4p/js/plugins/jasny-bootstrap.min.js')}}"></script>

    <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
    <script src="{{asset('md4p/js/plugins/fullcalendar.min.js')}}"></script>

    <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
    <script src="{{asset('md4p/js/plugins/jquery-jvectormap.js')}}"></script>

    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="{{asset('md4p/js/plugins/nouislider.min.js')}}" ></script>

    <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>

    <!-- Library for adding dinamically elements -->
    <script src="{{asset('md4p/js/plugins/arrive.min.js')}}"></script>

    <!--  Google Maps Plugin    -->
    {{--<script  src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>--}}

    <!-- Chartist JS -->
    <script src="{{asset('md4p/js/plugins/chartist.min.js')}}"></script>

    <!--  Notifications Plugin    -->
    <script src="{{asset('md4p/js/plugins/bootstrap-notify.js')}}"></script>

    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="{{asset('md4p/js/material-dashboard.min.js?v=2.1.1')}}" type="text/javascript"></script>

    @yield('js')

</body>
</html>
