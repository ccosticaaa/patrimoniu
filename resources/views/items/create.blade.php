@extends('layout')

@section('style')
    <link href="https://cdn.jsdelivr.net/npm/froala-editor@latest/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />

    <style>
        .resize-image {
            height: 250px;
            margin-left: auto;
            margin-right: auto;
        }
        .card-text-right:hover {
            cursor: pointer;
        }
        .card-text-right {
            position: relative!important;
            float: right!important;
            /*margin-top: -28px!important;*/
        }
        .custom-error-cross {
            right: -35px!important;
        }
        .ml-20 {
            margin-left: 20px;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <form id="create-item-form" class="form-horizontal" action="{{route('items.store')}}" method="POST" novalidate="novalidate" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-header card-header-primary card-header-text">
                            <div class="card-text">
                                <h4 class="card-title">Add a New Item</h4>
                            </div>
                            <div class="card-text card-text-right card-header-info" id="redirect-to-items-index">
                                <h4 class="card-title">Go Back</h4>
                            </div>
                            <a href="{{route('items.index')}}" class="btn btn-primary btn-round button-right d-none" id="items-index">Go Back</a>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <label class="col-sm-2 col-form-label" for="item-name">Name</label>
                                <div class="col-sm-7">
                                    <div class="form-group bmd-form-group @if ($errors->has('name')) has-danger @endif">
                                        <input class="form-control" value="{{old('name')}}" type="text" id="item-name" name="name" required="true" aria-required="true" aria-invalid="true">
                                        @if ($errors->has('name'))
                                            <span class="material-icons form-control-feedback custom-error-cross">clear</span>
                                                @foreach ($errors->get('name') as $key => $error)
                                                    <label id="name-error-{{$key}}" class="error" for="name"><strong>#{{++$key}}</strong> {{$error}}</label>
                                                @endforeach
                                        @endif
                                    </div>
                                </div>
                                <label class="col-sm-2 label-on-right ml-20">
                                    <code>required</code>
                                </label>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label" for="item-category">Category</label>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <select class="form-control selectpicker" data-style="btn btn-link" id="item-category" name="category_id">
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}" @if($category->id == old('category_id')) selected @endif>{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <label class="col-sm-2 label-on-right ml-20">
                                    <code>required</code>
                                </label>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Image Preview</label>
                                <div class="col-sm-7">
                                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <img class="d-block resize-image" src="{{asset('/uploads/items/img/placeholder2.png')}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">Image</label>
                                <div class="col-sm-7">
                                    <div class="form-group form-file-upload form-file-simple @if ($errors->has('image')) has-danger @endif">
                                        <input type="file" name="image" class="inputFileHidden" id="item-image-upload" accept="image/png, image/jpeg, image/jpg">
                                        <div class="input-group">
                                            <input type="text" class="form-control inputFileVisible" placeholder="Single File" id="item-image-input">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-fab btn-round btn-primary" id="item-image-button">
                                                    <i class="material-icons">attach_file</i>
                                                </button>
                                                <button type="button" class="btn btn-fab btn-round btn-danger" id="remove-item-image-button">
                                                    <i class="material-icons">delete</i>
                                                </button>
                                            </span>
                                        </div>
                                        @if ($errors->has('image'))
                                            <span class="material-icons form-control-feedback custom-error-cross">clear</span>
                                            @foreach ($errors->get('image') as $key => $error)
                                                <label id="image-error-{{$key}}" class="error" for="image"><strong>#{{++$key}}</strong> {{$error}}</label>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <label class="col-sm-2 label-on-right ml-20">
                                    <code>optional</code>
                                </label>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label" for="item-page-title">Title</label>
                                <div class="col-sm-7">
                                    <div class="form-group bmd-form-group @if ($errors->has('page_title')) has-danger @endif">
                                        <input class="form-control" type="text" id="item-page-title" name="page_title" required="true" value="{{old('page_title')}}">
                                        @if ($errors->has('page_title'))
                                            <span class="material-icons form-control-feedback custom-error-cross">clear</span>
                                            @foreach ($errors->get('page_title') as $key => $error)
                                                <label id="page_title-error-{{$key}}" class="error" for="page_title"><strong>#{{++$key}}</strong> {{$error}}</label>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <label class="col-sm-2 label-on-right ml-20">
                                    <code>required</code>
                                </label>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label" for="item-page-body">Content</label>
                                <div class="col-sm-7">
                                    <div class="form-group">
                                        <textarea class="form-control" id="item-page-body" name="page_body" required="true" rows="6">{{old('page_body')}}</textarea>
                                    </div>
                                </div>
                                <label class="col-sm-2 label-on-right ml-20">
                                    <code>required</code>
                                </label>
                            </div>
                        </div>
                        <div class="card-footer ml-auto mr-auto">
                            <button type="submit" id="add_item" class="btn btn-primary">Add Item</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/froala-editor@latest/js/froala_editor.pkgd.min.js"></script>

    <script>

        function filePreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.resize-image').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function () {

            let itemTextarea = null;

            itemTextarea = new FroalaEditor(
                '#item-page-body',
                {
                    heightMin: 100,
                }
            );

            let hideTextareaLogoInterval = setInterval(
                () => {
                    if (itemTextarea !== null)
                    {
                        $('a[id="logo"][href="https://froala.com/wysiwyg-editor"]').remove();
                        $('a[href="https://www.froala.com/wysiwyg-editor?k=u"]').parent('div').remove();
                        $('span.fr-placeholder').css('margin-top', '0px');
                        clearInterval(hideTextareaLogoInterval);
                    }
                },
                200
            );

            $('#item-image-button, #item-image-input').on('click', function () {
                $('#item-image-upload').trigger('click');
            });

            $('#item-image-upload').on('change', function () {
                var filename = $(this).val().split('\\').pop();
                $('#item-image-input').val(filename);
                filePreview(this);
            });

            $('#remove-item-image-button').on('click', function () {
                $('#item-image-upload').val('');
                $('#item-image-input').val('');
            });

            $('#redirect-to-items-index').on('click', function () {
                document.querySelector('#items-index').click();
            });

            $('label[id*="-error-"]').each(function (key, item) {
                $(item).removeClass('bmd-label-static');
            });

        });
    </script>
@endsection

