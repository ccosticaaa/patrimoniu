@extends('layout')

@section('style')
    <style>
        .card-icon-right {
            position: relative!important;
            float: right!important;
            margin-top: -64px!important;
            margin-left: 15px!important;
            margin-right: 0!important;
        }
        .card-icon-right:hover {
            cursor: pointer;
        }
        .button-right {
            position: relative!important;
            float: right!important;
            margin-top: -40px!important;
        }
        .resize-image {
            max-width: 200px;
            max-height: 200px;
        }
        .card-text-right {
            position: relative!important;
            float: right!important;
            margin-top: -28px!important;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header card-header-primary card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">apartment</i>
                        </div>
                        <h4 class="card-title">Items</h4>
                        <div class="card-icon card-icon-right card-header-info" id="redirect-to-create-item">
                            <i class="material-icons">library_add</i>
                        </div>
                        <h4 class="card-title card-text-right">Add Item</h4>
                        <a href="{{route('items.create')}}" class="btn btn-primary btn-round button-right d-none" id="create-item" >Add New</a>
                    </div>
                    <div class="card-body">
                        <div class="material-datatables">
                            <table class="table table-striped table-no-bordered table-hover" id="items-datatable" cellspacing="0" width="100%" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th width="10%">ID</th>
                                        <th width="15%">Name</th>
                                        <th width="15%">Category</th>
                                        <th>UUID</th>
                                        <th>Image</th>
                                        <th class="text-right">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($items as $item)
                                        <tr>
                                            <td>{{$item->id}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->category->name}}</td>
                                            <td>{{$item->uuid}}</td>
                                            <td>
                                                <img src="{{asset($item->image_path)}}" class="resize-image">
                                            </td>
                                            <td class="td-actions text-right">
                                                <a href="{{route('items.edit', $item)}}" id="edit_btn" type="button" rel="tooltip" class="btn btn-success btn-round">
                                                    <i class="material-icons">edit</i>
                                                </a>
                                                <a href="{{str_replace('.pdf', '_only_qr.pdf', $item->print_file_path)}}" id="pdf_btn" target="_blank" type="button" rel="tooltip" class="btn btn-info btn-round">
                                                    <i class="material-icons">cloud_download</i>
                                                </a>
                                                <a href="{{$item->print_file_path}}" id="print_btn" target="_blank" type="button" rel="tooltip" class="btn btn-danger btn-round">
                                                    <i class="material-icons">picture_as_pdf</i>
                                                </a>
                                                <a href="{{route('items.show', [$item])}}" id="show_btn" target="_blank" type="button" rel="tooltip" class="btn btn-default btn-round">
                                                    <i class="material-icons">remove_red_eye</i>
                                                </a>
                                                <a href="{{route('items.destroy', $item)}}" id="delete_btn" type="button" rel="tooltip" class="btn btn-danger btn-round">
                                                    <i class="material-icons">delete</i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(session('status'))
        <button class="btn btn-primary d-none" id="update-notification" data-notification-message="{{session('message')}}"></button>
    @endif
@endsection

@section('js')
    <script type="text/javascript">


        function showNotification(from, align, message){

            $.notify(
                {
                    icon: "add_alert",
                    message: message,
                },
                {
                    type: 'success',
                    allow_dismiss: false,
                    delay: 1500,
                    placement: {
                        from: from,
                        align: align
                    }
                }
            );
        }

        $(document).ready(function () {
            $('#items-datatable').DataTable({
                "pagingType": "full_numbers",
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                },
                order: [ [0, 'desc'] ],
                columnDefs: [
                    {
                        orderable: false,
                        targets: [
                            3,
                            4,
                            5,
                        ],
                    }
                ],
            });

            var table = $('#categories-datatable').DataTable();


            $('#redirect-to-create-item').on('click', function () {
                document.querySelector('#create-item').click();
            });

            let showNotificationButton = $('#update-notification');

            showNotificationButton.on('click', function () {
                let notificationMessage = $(this).data('notification-message');
                showNotification('top', 'center', notificationMessage);
            });

            let shouldDisplayNotification = showNotificationButton.length;
            if (shouldDisplayNotification)
            {
                showNotificationButton.trigger('click');
            }
        });
    </script>
@endsection

