@extends('pdf_layout')

@section('css')
    <style>
        .text-center {
            text-align: center;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <table style="width:100%;">
                <tr>
                    <td align="center">
                        <img src="{{asset($item['qr_code_path'])}}">
                    </td>
                </tr>
            </table>
        </div>
    </div>
@endsection