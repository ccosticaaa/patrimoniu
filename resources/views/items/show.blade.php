@extends("layout")
@section("style")
<style>
    .text-center-custom{
        text-align: center;
    }
    .text-left-custom{
        text-align: left;
    }
</style>
@endsection

@section("content")
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card card-nav-tabs text-center-custom">
                    <div class="card-header card-header-primary">
                        {{$item->page_title}}
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail">
                                        <img src="{{asset($item->image_path)}}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="card-text text-left-custom">{!! $item->page_body !!}</div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail">
                                        <img src="{{asset($item->qr_code_path)}}">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <a href="{{route('items.index')}}" class="btn btn-info">Go back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
