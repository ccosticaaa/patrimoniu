<div class="row">
    <div class="col-12">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h4 class="text-center">{{$item['page_title']}}</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <table style="width:100%;">
                        <tr>
                            <td align="center">
                                <img src="{{asset($item['image_path'])}}" height="250px">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    {!! $item['page_body'] !!}
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <table style="width:100%;">
                        <tr>
                            <td align="center">
                                <img src="{{asset($item['qr_code_path'])}}">
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>