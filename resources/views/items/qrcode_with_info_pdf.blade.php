@extends('pdf_layout')

@section('css')
    <style>
        .text-center {
            text-align: center;
        }
    </style>
@endsection

@section('content')
    @include('items.qrcode_with_info_pdf_template', [
        'item' => $item,
    ])
@endsection