@extends('layout')

@section('style')
    <style>
        .card-icon-right {
            position: relative!important;
            float: right!important;
            margin-top: -64px!important;
            margin-left: 15px!important;
            margin-right: 0!important;
        }
        .card-icon-right:hover {
            cursor: pointer;
        }
        .button-right {
            position: relative!important;
            float: right!important;
            margin-top: -40px!important;
        }
        .resize-image {
            max-width: 200px;
            max-height: 200px;
        }
        .card-text-right {
            position: relative!important;
            float: right!important;
            margin-top: -28px!important;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header card-header-primary card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">apartment</i>
                        </div>
                        <h4 class="card-title">Items</h4>
                        <div class="card-icon card-icon-right card-header-info" id="redirect-to-create-category">
                            <i class="material-icons">library_add</i>
                        </div>
                        <h4 class="card-title card-text-right">Add Category</h4>
                        <a href="{{route('categories.create')}}" class="btn btn-primary btn-round button-right d-none" id="create-category">Add New</a>
                    </div>
                    <div class="card-body">
                        <div class="material-datatables">
                            <table class="table table-striped table-no-bordered table-hover" id="categories-datatable" cellspacing="0" width="100%" style="width: 100%">
                                <thead>
                                <tr>
                                    <th width="10%">ID</th>
                                    <th>Name</th>
                                    <th>Items Linked</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categories as $category)
                                    <tr>
                                        <td>{{$category->id}}</td>
                                        <td>{{$category->name}}</td>
                                        <td>{{$category->items->count()}}</td>
                                        <td class="td-actions text-right">
                                            <a href="{{route('categories.edit', $category)}}" type="button" rel="tooltip" class="btn btn-success btn-round">
                                                <i class="material-icons">edit</i>
                                            </a>
                                            <a href="{{route('categories.destroy', $category)}}" type="button" rel="tooltip" class="btn btn-danger btn-round">
                                                <i class="material-icons">delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(session('status'))
        <button class="btn btn-primary d-none" id="update-notification" data-notification-message="{{session('message')}}"></button>
    @endif
@endsection

@section('js')
    <script type="text/javascript">

        function showNotification(from, align, message){

            $.notify(
                {
                    icon: "add_alert",
                    message: message,
                },
                {
                    type: 'success',
                    allow_dismiss: false,
                    delay: 1500,
                    placement: {
                        from: from,
                        align: align
                    }
                }
            );
        }

        $(document).ready(function () {
            $('#categories-datatable').DataTable({
                "pagingType": "full_numbers",
                "lengthMenu": [
                    [10, 25, 50, -1],
                    [10, 25, 50, "All"]
                ],
                responsive: true,
                language: {
                    search: "_INPUT_",
                    searchPlaceholder: "Search records",
                },
                order: [ [0, 'desc'] ],
                columnDefs: [
                    {
                        orderable: false,
                        targets: [
                            3,
                        ],
                    }
                ],
            });

            var table = $('#categories-datatable').DataTable();

            $('#redirect-to-create-category').on('click', function () {
                document.querySelector('#create-category').click();
            });

            let showNotificationButton = $('#update-notification');

            showNotificationButton.on('click', function () {
                let notificationMessage = $(this).data('notification-message');
                showNotification('top', 'center', notificationMessage);
            });

            let shouldDisplayNotification = showNotificationButton.length;
            if (shouldDisplayNotification)
            {
                showNotificationButton.trigger('click');
            }
        });
    </script>
@endsection

