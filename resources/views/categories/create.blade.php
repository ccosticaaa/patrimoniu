@extends('layout')

@section('style')
    <link href="https://cdn.jsdelivr.net/npm/froala-editor@latest/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />

    <style>
        .resize-image {
            height: 250px;
            margin-left: auto;
            margin-right: auto;
        }
        .card-text-right:hover {
            cursor: pointer;
        }
        .card-text-right {
            position: relative!important;
            float: right!important;
            /*margin-top: -28px!important;*/
        }
        .custom-error-cross {
            right: -35px!important;
        }
        .ml-20 {
            margin-left: 20px;
        }
    </style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <form id="create-category-form" class="form-horizontal" action="{{route('categories.store')}}" method="POST" novalidate="novalidate" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-header card-header-primary card-header-text">
                            <div class="card-text">
                                <h4 class="card-title">Add a New Category</h4>
                            </div>
                            <div class="card-text card-text-right card-header-info" id="redirect-to-categories-index">
                                <h4 class="card-title">Go Back</h4>
                            </div>
                            <a href="{{route('categories.index')}}" class="btn btn-primary btn-round button-right d-none" id="categories-index">Go Back</a>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <label class="col-sm-2 col-form-label" for="category-name">Name</label>
                                <div class="col-sm-7">
                                    <div class="form-group bmd-form-group @if ($errors->has('name')) has-danger @endif">
                                        <input class="form-control" value="{{old('name')}}" type="text" id="category-name" name="name" required="true" aria-required="true" aria-invalid="true">
                                        @if ($errors->has('name'))
                                            <span class="material-icons form-control-feedback custom-error-cross">clear</span>
                                            @foreach ($errors->get('name') as $key => $error)
                                                <label id="name-error-{{$key}}" class="error" for="name"><strong>#{{++$key}}</strong> {{$error}}</label>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <label class="col-sm-2 label-on-right ml-20">
                                    <code>required</code>
                                </label>
                            </div>

                        </div>
                        <div class="card-footer ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary">Add Category</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js')

    <script>



        $(document).ready(function () {

            $('#redirect-to-categories-index').on('click', function () {
                document.querySelector('#categories-index').click();
            });

            $('label[id*="-error-"]').each(function (key, item) {
                $(item).removeClass('bmd-label-static');
            });

        });
    </script>
@endsection

