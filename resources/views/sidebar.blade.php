<div class="sidebar" data-color="purple" data-background-color="black" data-image="{{asset('md4p/img/sidebar-1.jpg')}}">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
    <div class="logo">
        <a href="{{route('items.index')}}" class="simple-text logo-normal text-center">
            {{ config('app.name', 'Laravel') }}
        </a>
    </div>
    <div class="sidebar-wrapper ps-container ps-theme-default" data-ps-id="ea1a47fa-01a7-3e48-938c-923988e00230">
        @php $routeName = \Request::route()->getName(); @endphp
        <ul class="nav">

            <li class="nav-item @if (strpos($routeName, 'items') !== false) active @endif">
                <a class="nav-link" href="{{route('items.index')}}">
                    <i class="material-icons">apartment</i>
                    <p>Items</p>
                </a>
            </li>
            <li class="nav-item @if (strpos($routeName, 'categories') !== false) active @endif">
                <a class="nav-link" href="{{route('categories.index')}}">
                    <i class="material-icons">folder_special</i>
                    <p>Categories</p>
                </a>
            </li>
        </ul>
        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 0px;">
            <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;">
            </div>
        </div>
        <div class="ps-scrollbar-y-rail" style="top: 0px; right: 0px;">
            <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 0px;">
            </div>
        </div>
    </div>
</div>