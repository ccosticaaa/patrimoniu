<?php

if (!defined('ITEM_IMAGE_FULL_PATH'))
{
    define ('ITEM_IMAGE_FULL_PATH', base_path() . '/public/uploads/items/img/');
}
if (!defined('ITEM_IMAGE_PATH'))
{
    define ('ITEM_IMAGE_PATH', '/uploads/items/img/');
}
if (!defined('__DB__ITEM_IMAGE_PATH'))
{
    define ('__DB__ITEM_IMAGE_PATH', '/uploads/items/img');
}

if (!defined('PLACEHOLDER_DEFAULT_IMAGE_NAME'))
{
    define('PLACEHOLDER_DEFAULT_IMAGE_NAME', 'placeholder2.png');
}
if (!defined('PLACEHOLDER_IMAGE_FULL_PATH'))
{
    define('PLACEHOLDER_IMAGE_FULL_PATH', base_path() . '/public/uploads/items/img/');
}
if (!defined('PLACEHOLDER_DEFAULT_IMAGE_PATH'))
{
    define('PLACEHOLDER_DEFAULT_IMAGE_PATH', ITEM_IMAGE_PATH . PLACEHOLDER_DEFAULT_IMAGE_NAME);
}

if (!defined('QRCODE_IMAGES_FULL_PATH'))
{
    define('QRCODE_IMAGES_FULL_PATH',  base_path() . '/public/uploads/items/qrcodes/');
}
if (!defined('QRCODE_IMAGES_PATH'))
{
    define('QRCODE_IMAGES_PATH', '/uploads/items/qrcodes/');
}

if (!defined('ITEM_PDF_FULL_PATH'))
{
    define('ITEM_PDF_FULL_PATH',  base_path() . '/public/uploads/items/pdfs/');
}
if (!defined('ITEM_PDF_PATH'))
{
    define('ITEM_PDF_PATH', '/uploads/items/pdfs/');
}

if (!defined('QR_CODE_SIZE'))
{
    // 300px is decent, we could go higher for pdf formats and such
    define('QR_CODE_SIZE', 300);
}
if (!defined('QR_CODE_MARGINS'))
{
    define('QR_CODE_MARGINS', 1);
}

if (!defined('DEFAULT_CATEGORY_ID'))
{
    define('DEFAULT_CATEGORY_ID', 1);
}







