<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(
    [

    ],
    function () {
        Auth::routes(['register' => false]);
    }
);

Route::group(
    [
        'middleware' => [
            'auth',
        ],
    ],
    function () {
        Route::group(
            [
                'prefix' => 'items',
                'as' => 'items.',
                'namespace' => '\Admin\Item',
            ],
            function () {
                //items routes
                Route::get('', 'ItemController@index')->name('index');
                Route::get('/create', 'ItemController@create')->name('create');
                Route::post('', 'ItemController@store')->name('store');
                Route::get('/{item}/show', 'ItemController@show')->name('show');
                Route::get('/{item}/edit', 'ItemController@edit')->name('edit');
                Route::match(['put', 'patch'], '/{item}', 'ItemController@update')->name('update');
                Route::get('/{item}/delete', 'ItemController@destroy')->name('destroy');
            }
        );
        Route::group(
            [
                'prefix' => 'categories',
                'as' => 'categories.',
                'namespace' => '\Admin\Category',
            ],
            function () {
                //categories routes
                Route::get('', 'CategoryController@index')->name('index');
                Route::get('/create', 'CategoryController@create')->name('create');
                Route::post('', 'CategoryController@store')->name('store');
                //Route::get('/{category}', 'CategoryController@show')->name('show');
                Route::get('/{category}/edit', 'CategoryController@edit')->name('edit');
                Route::match(['put', 'patch'], '/{category}', 'CategoryController@update')->name('update');
                Route::get('/{category}/delete', 'CategoryController@destroy')->name('destroy');
            }
        );
    }
);

Route::group([
    'middleware' => ['cors'],
], function ($router) {

    Route::get('/{itemUUID}', 'Controller@getItemInformation')->name('item.view');
});

Route::any('/{any}', function () {
    return redirect()->route('items.index');
})->where('any', '.*');

